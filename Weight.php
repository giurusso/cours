<?php

final class Weight
{
    private $weight;

    public function __construct(int $weight)
    {
        if ($weight < 0){
            throw new InvalidArgumentException("Weight cannot contain any letter and there must be at least one character");
        }
        $this->weight = $weight;
    }

    public function asInteger(): int
    {
        return $this->weight;
    }
}

