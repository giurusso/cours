<?php


final class Age
{
    private $age;


    public function __construct(int $age)
    {
        if ($age < 0) {
            throw new InvalidArgumentException("L'âge ne peut pas être négatif");
        }
        $this->age = $age;
    }

    public function asInteger() : int
    {
        return $this->age;
    }
}
