<?php


final class Url
{
    private $url;

    private $pattern =
        '/(?<scheme>^(?:https?|s?ftp):\/\/)'.  // scheme
        '(?<domain>(?:(?:[a-zA-Z0-9]+-?[a-zA-Z0-9]+)+\.){1,4})'. //sub-domains/domain (4 max)
        '(?<extension>[a-zA-Z]{2,4}\/?)'. // extension
        '(?<path>(?:\/[a-zA-Z0-9@\!\$&\(\)\*\+,;=_~:\/?%\#\[\].-]*)*)'. //path
        '$/';


    public function __construct(string $url)
    {
//        if (preg_match($this->pattern, $url) !== 1){
//            throw new InvalidArgumentException('Invalid URL man on the street side');
//        }

        $this->url = $url;
    }

    public function asString(): string
    {
        return $this->url;
    }

    public function showDump(): string
    {
        return $this->pattern;
    }
}

//^(https?|ftps?):\/\/([a-z0-9:=?;\/@&,_\(\)%#\[\]!+$~]+([a-z]+-[a-z]+)*\.?[a-z0-9:=?;\/@&,_\(\)%#\[\]!+$~]+)*\.[a-z\/]{2,5}$

//doit valider les schemes suivants : http, https, ftp
//Un nom de domaine avec :
//- de 0 à 3 sous-domaines
//- un nom de domaine
//- une extension de 2 à 4 caractères
//- caractères autorisés : lettres, chiffres et - (en milieu de mots seulement)
//Un chemin, c'est à dire plusieurs éléments séparés par des slashs (/) et pourvant se terminer par un slash final, contenant soit des lettres (minuscules et majuscules) soit des chiffres, ainsi que les caractères suivants : @!$&()*+,;=_~:/?%-.#[]'


//Domain names may be formed from the set of alphanumeric ASCII characters (a-z, A-Z, 0-9), but characters are case-insensitive. In addition, the hyphen is permitted if it is surrounded by characters, digits or hyphens, although it is not to start or end a label.

