<?php

final class Email
{
    private $email;

    public function __construct(string $email)
    {
        if (preg_match('#^[a-z1-9-_]+@[a-z]+\.[a-z]+$#', $email) !== 1){
            throw new InvalidArgumentException('Invalid email');
        }

        $this->email = $email;
    }

    public function asString(): string
    {
        return $this->email;
    }
}
