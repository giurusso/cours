<?php


final class Name
{
    private $name;

    public function __construct(string $name)
    {

        if (preg_match('#^[a-z-]+$#i', $name) !== 1){
            throw new InvalidArgumentException("The name cannot contain numbers and there must be at least one character");
        }
        $this->name = $name;
    }

    public function asString() : string
    {
        return $this->name;
    }
}
